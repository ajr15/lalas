import sys
import argparse as ap

from . import molloader
from . import knotidentifier
from . import pathfinder
from . import getlalas


__all__ = ['main']

def main():

    # how much the covalent bond can be longer by than the sum of covalent radii
    covalency_factor = float(1.3)

    # parse arguments + read input file + set molgraph
    args = parse_args()
    infile = args['infile']
    molrepr = molloader.load(infile)
        
    # rotate molecule into XY plane
    molrepr.align_to_xy_plane()

    # identify knots
    knotrepr, edges = knotidentifier.identify(molrepr, covalency_factor)
    assert len(knotrepr) > 2, "\nNot enough knots found, need at least 3."

    # find path through knots
    paths, graph, knotedges = pathfinder.find(knotrepr)

    getlalas.get_LALAS(paths, knotedges, knotrepr, graph)
    
    sys.exit(0)



def parse_args():
    """
    Argument parsing
    """
    parser = ap.ArgumentParser(description=f"Code to obtain LALAS representation for inputed PBH.")

    parser.add_argument("infile", metavar="INFILE", help="Input file. Supported types: .xyz and .in (Gaussian input)", type=str, default=None)
       
    args = vars(parser.parse_args())    
    
    return args


if __name__ == '__main__':
    main()
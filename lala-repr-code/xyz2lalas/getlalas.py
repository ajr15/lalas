"""
Code to recursively go through network of knots (binary tree) and give out an array of Knot numbers.

It is a BINARY tree, so it can only branch once each time.

"""

import copy
import numpy as np


class Node():

    def __init__(self, _index):
        self.index = _index
        self.annulation = None
        self.children = []

    def count_children(self):
        return len(self.children)

    def __str__(self):
        return f"Node(index = {self.index}, annulation = {self.annulation}, #children = {len(self.children)})"


def get_LALAS(_paths, _edges, _knots, _graph):

    longest_path = [knot.index for knot in find_longest_path(_paths, _graph).route] # find the longest route

    root = Node(longest_path[0]) # set the first knot in the longest path as root node

    edges = copy.deepcopy(_edges) # we will be popping elements off, so we need a deepcopy

    build_tree(root, edges) # build the tree recursively

    get_annulation(root, None, _knots) # find annulations recursively

    sequence = get_sequence(longest_path, root) # get the sequence to print

    print("".join(sequence))


def find_longest_path(_paths, _graph):
    # look for longest path(s)
    longest_paths = []
    longest_l = 0
    for path in _paths:
        l = len(path.route)
        if l > longest_l:
            longest_l = l
            longest_paths = [path]
        elif l == longest_l:
            longest_paths.append(path)
    
    # look for longest path with highest number of branches
    paths_branches = []
    if len(longest_paths) > 1:
        for path in longest_paths:
            nbranches = 0
            l = len(path.route)
            for knot in path.route:
                degree = _graph.degree[knot.index]
                if degree > 2:
                    nbranches += 1
            paths_branches.append(nbranches)
        
    max_branches_index = paths_branches.index(max(paths_branches))
    return longest_paths[max_branches_index]  


def build_tree(current_node, edges):
    # recursive tree building

    # breaking condition: we're out of edges
    if len(edges) < 1:
        return

    # run through edges and find all children
    for i in range(len(edges) -1, -1, -1):
        if current_node.index == edges[i][0]:
            current_node.children.append(Node(edges[i][1]))
            edges.pop(i)
        elif current_node.index == edges[i][1]:
            current_node.children.append(Node(edges[i][0]))
            edges.pop(i)

    # recursion
    for child in current_node.children:
        build_tree(child, edges)

    
def get_annulation(current_node, previous_node, _knots):
    if len(current_node.children) < 1:
        return # nothing to do, found an end

    elif len(current_node.children) > 1: # found a branch, must be "a"
        current_node.annulation = "a"
        for child in current_node.children:
            get_annulation(child, current_node, _knots)
    else: # prevent this from being called on the root node
        if previous_node:
            if get_angle(np.array(_knots[previous_node.index].get_coord()), np.array(_knots[current_node.index].get_coord()), np.array(_knots[current_node.children[0].index].get_coord())) < -0.87:
                current_node.annulation = "l"
            else:
                current_node.annulation = "a"
        get_annulation(current_node.children[0], current_node, _knots)


def get_sequence(path, current_node):

    sequence = []

    # no branching 
    while current_node.count_children() == 1:
        if current_node.annulation: sequence.append(current_node.annulation)
        current_node = current_node.children[0]
    
    # branching
    # currently we do not check for longest branch in branches
    if current_node.count_children() > 1:
        if current_node.annulation: sequence.append(current_node.annulation)
        sequence.append("(")
        if current_node.children[1].index in path:
            sequence.extend(get_sequence(path, current_node.children[0]))
            sequence.append(")")
            sequence.extend(get_sequence(path, current_node.children[1]))
        else:
            sequence.extend(get_sequence(path, current_node.children[1]))
            sequence.append(")")
            sequence.extend(get_sequence(path, current_node.children[0]))

    # break condition
    if current_node.count_children() < 1:
        if current_node.annulation: sequence.append(current_node.annulation)
        return sequence
    
    return sequence

def print_tree(root):

    def rec_print(node):
        print(node)
        for child in node.children:
            rec_print(child)

    rec_print(root)


def get_angle(a, b, c):
    """
    get_angle(a: numpy.ndarray, b: numpy.ndarray, c: numpy.ndarray) -> numpy.float64

    Get cosine of an angle abc given 3 points in space a, b, and c with (x,y,z) coordinates.

    in:
    a, b, c: 3 numpy arrays containing the (x,y,z) coordinates of points a, b, c.

    out:
    float that is the cosine of the angle between vectors ab and bc.

    """

    ba = a - b
    bc = c - b

    return np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))

# Revealing Structure-Property Relationships in Polybenzenoid Hydrocarbons with Interpretable Machine-Learning

## Content
* Data used as input (SMILES, LALAS, properties, structural features)
* Source code for calculations
* Source code for LALAS representation

## How to cite this work
If you use any part of this work, please cite the following:
S. Fite, A. Wahab, E. Paenurk, Z. Gross, and R. Gershoni-Poranne, Revealing Structure-Property Relationships in Polybenzenoid Hydrocarbons with Interpretable Machine-Learning, DOI: 10.26434/chemrxiv-2022-6dd6n

## Support
For support or to report any issues, please contact: porannegroup /at/ technion.ac.il

## Authors and acknowledgment
This work was conducted under the supervision of Prof. Dr. Renana Gershoni-Poranne (Technion/ETH Zurich). The following people contributed to this project: 
1. Shachar Fite (Technion)
2. Alexandra Wahab (ETH Zurich)
3. Dr. Eno Paenurk (ETH Zurich)

The invaluable assistance of the following people is gratefully acknowledged: Prof. Dr. Peter Chen, Prof. Zeev Gross, Dr. Alexandra Tsybizova, Felix Fleckenstein. 

In addition, the financial support of the Branco Weiss Fellowship is acknowledged.

## License
The code and data in this repository are provided free-of-charge. They are licensed under a CC-BY-NC-SA license. Please cite the relevant literature if you use them.
